# Fatigue and Stress

Both Fatigue and Stress are confined to a scale from 0-100. If your fatigue and/or stress get too high, your monster will start to lose additional lifespan,
as well as that which is caused by aging. The product of the calculation to determine this is called **Lifespan Index**.

### Fatigue Messages

Fatigue messages will be given at the beginning of every week.

| Fatigue Value | Colt's Message                                          |
| ------------- | ------------------------------------------------------- |
| 0             | `Monster Name` is very well.                            |
| 1-19          | `Monster Name` is well.                                 |
| 20-39         | `Monster Name` seems well.                              |
| 40-59         | `Monster Name` seems tired.                             |
| 60-79         | `Monster Name` seems pretty tired. Give it some rest.   |
| 80-95         | Oh...... `Monster Name` seems very tired.               |
| 96-100        | You are too hard on it... `Monster Name` will pass out. |

### Stress Messages

Stress messages are only given at the beginning of every **month**.

| Stress Value | Colt's Message                                                |
| ------------ | ------------------------------------------------------------- |
| 0-19         | No message.                                                   |
| 20-100       | `Monster Name` is stressed out. We have to take care of it... |

## Lifespan Index

LI is calculated at the end of each week, as the screen fades to black. The calculation is:  
**LI = Fatigue + Stress \* 2**

### Examples

**30 Fatigue, 15 Stress**

```
LI = 30 + 15 * 2 = 60;
```

**15 Fatigue, 30 Stress**

```
LI = 15 + 30 * 2 = 60
```

**LI Calculator**

<LiCalculator />

Your monster will start to lose additional weeks of lifespan at an LI of 70, then lifespan loss will increase from there.

| LI      | Lifespan lost          |
| ------- | ---------------------- |
| 0-69    | -1 (For living 1 week) |
| 70-104  | -2                     |
| 105-139 | -3                     |
| 140-174 | -4                     |
| 175-209 | -5                     |
| 210-244 | -6                     |
| 245-269 | -7                     |
| 270-300 | -8                     |

## Managing LI

### Training and Errantry

| Affected Value        | Light Drill | Heavy Drill | Errantry (1 week) | Errantry (Total) |
| --------------------- | ----------- | ----------- | ----------------- | ---------------- |
| **Fatigue**           | +10         | +15         | +18               | +72              |
| **Stress**            | +5          | +12         | +7                | +28              |
| **Total LI Increase** | +20         | +39         | +32               | +128             |

#### Effects of Greats and Cheats

Greats and Cheats will affect these values.

A Great on a Light Drill will add 5-9 Fatigue (randomly selected) instead of the usual 10, and on a Heavy Drill will add 10-14 Fatigue instead of 15. Greats do not affect the accumulation of stress.

Failure to praise Greats, or scolding Cheats, will add stress to your monster. The amount differs depending on your monster's [nature](./form-nature.md#nature), whereby monsters with a lower (worse) nature will gain more stress.

#### Errantry Bug

There is a bug in errantry whereby the final week is lived twice. Assuming a start point of 0 fatigue and 0 stress, the total loss should look like:

| Week  | Fatigue | Stress | LI  | Total Lifespan Lost |
| ----- | ------- | ------ | --- | ------------------- |
| **1** | 18      | 7      | 32  | -1                  |
| **2** | 36      | 14     | 64  | -1                  |
| **3** | 54      | 21     | 96  | -2                  |
| **4** | 72      | 28     | 128 | -3                  |

Totalling the right hand column results in -7 weeks, but in actual fact you live the 4th week twice and lose those 3 weeks (1 from aging, 2 from LI) again. The end result is that at the absolute best case your monster finishes errantry 10 weeks older than it started it.

### Battle

Battle causes a direct loss of 3 weeks of lifespan to your monster, in addition to the 1 week lost for living. How fatigued your monsters gets from battles is determined by its [form](./form-nature.md#form).

::: warning
This table needs better clarification on the distinction between the 3 sections.
:::

| Tournament Result | Fatigue        | Stress |
| ----------------- | -------------- | ------ |
| Winner            | 30 + Form / 13 | -45    |
| Intermediate      | 40 + Form / 10 | -38    |
| Loser             | 50 + Form / 8  | -30    |

Refusing to praise your monster after a win will increase your monster's stress by **8**. Refusing to give your monster the item it requests will increase its stress by **10**.

The IMa-FIMBA tournament is a special case which induces more fatigue and a different amount of stress than normal.

| Form | Fatigue | Stress |
| ---- | ------- | ------ |
| -100 | +38     | -30    |
| -50  | +44     | -35    |
| 0    | +50     | -40    |
| 50   | +56     | -45    |
| 100  | +62     | -50    |

### Expedition

Expeditions will give your monster **+70** Fatigue. This is only calculated after your monster returns, so the end result is that you lose the 4 weeks that the monster is away, plus 1 additional week for having 70 LI upon your return.

### Rest

Rest allows your monster to recover both fatigue and stress. The amount recovered is random, and the range of values it can take is based on **only** on your monster's life stage. It is a very [common misconception](/misc/common-misconceptions.md) that the Fire Stone and Lump of Ice increase the effectivness of resting, as this was belived to be the case for many years.

| Life Stage | Fatigue    | Stress    |
| ---------- | ---------- | --------- |
| **1**      | -36 to -39 | -5 to -8  |
| **2**      | -38 to -43 | -5 to -8  |
| **3**      | -40 to -47 | -5 to -10 |
| **4**      | -42 to -51 | -7 to -10 |
| **5**      | -44 to -55 | -9 to -12 |
| **6**      | -44 to -55 | -9 to -12 |
| **7**      | -40 to -47 | -5 to -10 |
| **8**      | -32 to -39 | -5 to -8  |
| **9**      | -32 to -37 | -5 to -8  |
| **10**     | -32 to -35 | -5 to -6  |
