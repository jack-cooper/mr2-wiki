# Form and Nature

Form and Nature work slightly differently to every other value associated with your monster, as they are confined to a larger scale, ranging from -100 to 100.

## Form

Form represents your monster's weight. Form's main effect is to modify your monster's `Spd` and `Def` stats while in battle. It also affects how much fatigue and stress your monster [receives from taking part in battles](./fatigue-stress.md#battle). Form should **not** be taken into account when assessing your monster's stats for the purposes of [correction](/combination/advanced.md#correction). The monster info page will display 1 of 5 potential values to help you gauge your form:

| Form        | Displayed Value |
|-------------|-----------------|
| -100 to -60 | Skinny          |
| -59 to -20  | Slim            |
| -19 to 19   | Normal          |
| 20 to 59    | Fat             |
| 60 to 100   | Plump           |

### Calculating exact Spd and Def
To calculate your monster's effective `Spd` and `Def`, divide your Form value by 4 and use this value as a percentage modifier for `Def`, and use the inverse of the value as a percentage modifier for `Spd`. e.g.

**At -100 Form with 760 Spd and 500 Def**

-100 / 4 = -25

So our `Def` modifier is -25%, while our `Spd` modifier is +25%

`760 * 1.25 =`950 effective `Spd`  
`500 * 0.75 =`375 effective `Def`

This modifier can **not** put form above level 20. This means that once `Spd` and `Def` are at very high levels, form can not add to them but can still negatively impact your monster's stats. Once both `Spd` and `Def` are at 950+, any form other than Normal, and even the far ends of the Normal spectrum, will very likely be detrimental. To ensure no stat loss, getting both `Spd` and `Def` to 999 with a Normal form guarantees that both stats will be effectively Level 20.

### Changing Form

Most actions that you do on a week will alter your monster's Form. Additionally, feeding at the beginning of every month, as well as many of the miscellaneous items which a monster requests, will alter a monster's Form by a marginal amount. This section will cover the weekly actions impacting Form. For a full list of items and their effects see [Items](/items/).

| Action/Item       | Effect on Form | Comments                                    |
|-------------------|---------------:|---------------------------------------------|
| Light Drill       | -1             | -                                           |
| Heavy Drill       | -2             | -                                           |
| Rest              | +5             | -                                           |
| Errantry          | -3             | -12 total                                   |
| Tournament Battle | -2             | Per battle, no change if opponent gives up. |

## Nature

Nature is a measure of how good or evil your monster is. It affects the likelihood of Greats and Cheats during training, technique acquisition at [errantry](/errantry/), which items you can find during an [expedition](/expedition/), and whether you have either (or neither) of the `Power` and `Anger` [battle specials](/tourneys/battle-specials.md). Just as with Form, the in-game menu will give you a rough gauge of your monster's Nature.

| Nature      | Displayed Value |
|-------------|-----------------|
| -100 to -60 | Worst           |
| -59 to -20  | Bad             |
| -19 to 19   | Neutral         |
| 20 to 59    | Good            |
| 60 to 100   | Best            |

### Good and Evil Techniques

Some techniques have requirements of a specific nature to be acquired. These requirements only affect acquisition at [errantry](/errantry/), tech inheritance from [combination](/combination/advanced.md) completely bypasses nature requirements. The actual nature required is specific to each individual tech, with some requiring as low as -60, while others require +50.

### Base Nature

Each monster has a base nature, responsible for defining the nature that paticular breed starts with. Although these follow general rules (Centaur, Tiger good :thumbsup: Joker, Naga bad :thumbsdown:), there isn't a lot of consistency on the actual numeric values. A monster's nature can not under any circumstances deviate more than 100 from its base nature, and once determined the base nature itself can not be changed by any means (excluding one very specific [rare occasion](/breeds/cocooning.md#mechanics)). This can cause problems, as under normal circumstances, if we look at Plant/Tiger as an example:

**Base Nature:** +70  
**Minimum Possible Nature:** 70 - 100 = -30

However, Plant's `Life Steal`, a highly desirable move, requires a Nature of -50 or lower to acquire! So a regular Plant/Tiger is incapable of picking up one of the Plant's strongest moves. To rectify this, as with most things in this game, we can use the power of [combination](/combination/advanced.md). In theory, the ideal base nature is 0  as this would allow us to hit the entire range of -100 to 100, although in practice this is often unnecessary with careful planning.

### Changing Nature

Unfortunately, the way in which Nature is implemented in the game's code is incredibly confusing and even now, over 2 decades after the game's release, we are still unsure on the precise values affecting it. That being said, we're close enough in every situation that matters, and thankfully unlike with Form, Nature's effects only really scale to around -60 and +60, so we can use the game to reliably tell us when we are at Best and Worst. The following is a list of things which affect Nature. Most [items](/items/) have been excluded for brevity, although Sweet and Sour Jellies, and the Hero and Heel badges, are worth a special mention here as being particularly good at modifying Nature.

#### Positive Effects

- Feed Sour Jelly
- Praise a Great
- Scold a Cheat
- Defeat an enemy monster at errantry
- Play with your monster
- Hold the Hero Badge in your inventory (passive)

#### Negative Effects

- Feed Sweet Jelly
- Don't praise a Great
- Forgive a Cheat
- Scold a Failure
- Hold the Heel Badge in your inventory (passive)