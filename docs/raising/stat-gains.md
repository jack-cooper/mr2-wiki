# Stat Gains

Each monster breed has its own set of gains for each of the 6 stats. These are represented on a 0-4 scale in the internal game data, although they are often referred to using a 1-5 scale in more human readable formats. All of the following examples use the 1-5 scale, where 1 is the worst gain possible (e.g. Golem's Skl and Spd), while 5 is the best gain possible (e.g. Golem's Pow and Def).

In each of the below tables, the columns (1-5) represent stat gains while the rows (1-10) represent the monster's life stage. Each of the values in a given cell has an equal chance of being selected.

## Light Drill

| Life Stage  | Stat Gain 1    | Stat Gain 2    | Stat Gain 3    | Stat Gain 4    | Stat Gain 5    |
|:-----------:|---------------:|---------------:|---------------:|---------------:|---------------:|
| **1**       |   1, 1, 2, 3   |   1, 1, 2, 3   |   1, 2, 3, 4   |   2, 3, 4, 5   |   3, 4, 5, 6   |
| **2**       |   1, 2, 3, 4   |   2, 3, 4, 5   |   3, 4, 5, 6   |   4, 5, 6, 7   |   6, 7, 8, 9   |
| **3**       |   2, 3, 4, 5   |   3, 4, 5, 6   |   5, 6, 7, 8   |   7, 8, 9, 10  | 9, 10, 11, 12  |
| **4**       |   2, 3, 4, 5   |   4, 5, 6, 7   |   6, 7, 8, 9   |  8, 9, 10, 11  | 11, 12, 13, 14 |
| **5**       |   4, 5, 6, 7   |   6, 7, 8, 9   |  8, 9, 10, 11  | 11, 12, 13, 14 | 14, 15, 15, 15 |
| **6**       |   3, 4, 5, 6   |   5, 6, 7, 8   |   7, 8, 9, 10  | 10, 11, 12, 13 | 13, 14, 15, 15 |
| **7**       |   2, 3, 4, 5   |   4, 5, 6, 7   |   6, 7, 8, 9   |  8, 9, 10, 11  | 11, 12, 13, 14 |
| **8**       |   2, 3, 4, 5   |   3, 4, 5, 6   |   5, 6, 7, 8   |   7, 8, 9, 10  | 9, 10, 11, 12  |
| **9**       |   1, 2, 3, 4   |   2, 3, 4, 5   |   3, 4, 5, 6   |   4, 5, 6, 7   |   6, 7, 8, 9   |
| **10**      |  1, 1, 2, 3    |   1, 1, 2, 3   |   1, 2, 3, 4   |   2, 3, 4, 5   |   3, 4, 5, 6   |

## Heavy Drill

The distribution of gains for the major and minor stat in a heavy drill is not indepdendent. There are only 4 possible outcomes.

| Major Stat         | Minor Stat         | Negatively Affected Stat |
|--------------------|--------------------|-------------------------:|
| Lowest Result      | 3rd Highest Result | -2                       |
| 3rd Highest Result | 2nd Highest Result | -3                       |
| 2nd Highest Result | Highest Result     | -2                       |
| Highest Result     | Lowest Result      | -3                       |

::: warning
This table assumes that the minor stat has 4 distinct results, I am currently unsure how fewer results affects the outcome. 1 is obvious and 2 will probably be logical but in cases where there are 3, e.g. Life stage 3, Stat gain 2, the answer is less clear.
:::
### Major Stat

| Stage       | Stat Gain 1    | Stat Gain 2    | Stat Gain 3    | Stat Gain 4    | Stat Gain 5    |
|:-----------:|---------------:|---------------:|---------------:|---------------:|---------------:|
| **1**       |   2, 2, 3, 4   |   3, 3, 4, 5   |   3, 4, 5, 6   |   5, 6, 7, 8,  |   6, 7, 8, 9   |
| **2**       |   3, 3, 4, 5   |   4, 5, 6, 7   |   6, 7, 8, 9   |  8, 9, 10, 11  | 11, 12, 13, 14 |
| **3**       |   4, 5, 6, 7   |   6, 7, 8, 9   | 9, 10 ,11, 12  | 12, 13, 14, 15 | 15, 16, 17, 18 |
| **4**       |   5, 6, 7, 8   |  8, 9, 10, 11  | 11, 12, 13, 14 | 15, 16, 17, 18 | 19, 20, 20, 20 |
| **5**       |  7, 8, 9, 10   | 11, 12, 13, 14 | 14, 15, 16, 17 | 19, 20, 20, 20 | 20, 20, 20, 20 |
| **6**       |   6, 7, 8, 9   | 10, 11, 12, 13 | 13, 14, 15, 16 | 18, 19, 20, 20 | 20, 20, 20, 20 |
| **7**       |   5, 6, 7, 8   |  8, 9, 10, 11  | 11, 12, 13, 14 | 15, 16, 17, 18 | 19, 20, 20, 20 |
| **8**       |   4, 5, 6, 7   |   6, 7, 8, 9   | 9, 10 ,11, 12  | 12, 13, 14, 15 | 15, 16, 17, 18 |
| **9**       |   3, 3, 4, 5   |   4, 5, 6, 7   |   6, 7, 8, 9   |  8, 9, 10, 11  | 11, 12, 13, 14 |
| **10**      |   2, 2, 3, 4   |   3, 3, 4, 5   |   3, 4, 5, 6   |   5, 6, 7, 8,  |   6, 7, 8, 9   |

### Minor Stat

| Stage       | Stat Gain 1 | Stat Gain 2 | Stat Gain 3 | Stat Gain 4 | Stat Gain 5 |
|:-----------:|------------:|------------:|------------:|------------:|------------:|
| **1**       | 2           | 2           | 2, 3        | 2, 3        | 2, 3, 4     |
| **2**       | 2           | 2, 3        | 2, 3, 4     | 2, 3, 4, 5  | 2, 3, 4, 5  |
| **3**       | 2, 3        | 2, 3, 4     | 2, 3, 4, 5  | 3, 4, 5, 6  | 4, 5, 6, 7  |
| **4**       | 2, 3        | 2, 3, 4     | 3, 4, 5, 6  | 4, 5, 6, 7  | 5, 6, 7, 8  |
| **5**       | 2, 3, 4     | 2, 3, 4, 5  | 4, 5, 6, 7  | 6, 7, 8, 9  | 7, 8, 9, 10 |
| **6**       | 2, 3, 4     | 2, 3, 4, 5  | 3, 4, 5, 6  | 5, 6, 7, 8  | 7, 8, 9, 10 |
| **7**       | 2, 3        | 2, 3, 4     | 3, 4, 5, 6  | 4, 5, 6, 7  | 5, 6, 7, 8  |
| **8**       | 2, 3        | 2, 3, 4     | 2, 3, 4, 5  | 3, 4, 5, 6  | 4, 5, 6, 7  |
| **9**       | 2           | 2, 3        | 2, 3, 4     | 2, 3, 4, 5  | 2, 3, 4, 5  |
| **10**      | 2           | 2, 3        | 2, 3        | 2, 3        | 2, 3, 4     |

## Errantry

Errantries behave similarly to regular training, except:

- There are 5 (still equally likely) possible values for the stat increases
- The increase in `Lif` is unaffected by your monster's `Lif` stat gain

### Major Stat

| LS          |       1       |        2        |        3         |         4          |          5         |
|:-----------:|--------------:|----------------:|-----------------:|-------------------:|-------------------:|
| **1**       | 2, 2, 2, 3, 4 | 2, 2, 3, 4, 5   | 2, 2, 3, 4, 5    | 2, 3, 4, 5, 6      | 3, 4, 5, 6, 7      |
| **2**       | 2, 2, 3, 4, 5 | 2, 3, 4, 5, 6   | 3, 4, 5, 6, 7    | 4, 5, 6, 7, 8      | 5, 6, 7, 8, 9      |
| **3**       | 3, 4, 5, 6, 7 | 4, 5, 6, 7, 8   | 5, 6, 7, 8, 9    | 6, 7, 8, 9, 10     | 8, 9, 10, 11, 12   |
| **4**       | 3, 4, 5, 6, 7 | 4, 5, 6, 7, 8   | 5, 6, 7, 8, 9    | 6, 7, 8, 9, 10     | 8, 9, 10, 11, 12   |
| **5**       | 5, 6, 7, 8, 9 | 7, 8, 9, 10, 11 | 8, 9, 10, 11, 12 | 10, 11, 12, 13, 14 | 13, 14, 15, 15, 15 |
| **6**       | 4, 5, 6, 7, 8 | 6, 7, 8, 9, 10  | 7, 8, 9, 10, 11  | 9, 10, 11, 12, 13  | 11, 12, 13, 14, 15 |
| **7**       | 3, 4, 5, 6, 7 | 5, 6, 7, 8, 9   | 6, 7, 8, 9, 10   | 7, 8, 9, 10, 11    | 9, 10, 11 , 12 13  |
| **8**       | 3, 4, 5, 6, 7 | 4, 5, 6, 7, 8   | 5, 6, 7, 8, 9    | 6, 7, 8, 9, 10     | 8, 9, 10, 11, 12   |
| **9**       | 2, 2, 3, 4, 5 | 2, 3, 4, 5, 6   | 3, 4, 5, 6, 7    | 4, 5, 6, 7, 8      | 5, 6, 7, 8, 9      |
| **10**      | 2, 2, 2, 3, 4 | 2, 2, 3, 4, 5   | 2, 2, 3, 4, 5    | 2, 3, 4, 5, 6      | 3, 4, 5, 6, 7      |

### Minor Stat (Lif)

| Life Stage  | Stat Increase |
|:-----------:|--------------:|
| **1**       | 1, 1, 1, 2, 3 |
| **2**       | 1, 1, 2, 3, 4 |
| **3**       | 1, 2, 3, 4, 5 |
| **4**       | 1, 2, 3, 4, 5 |
| **5**       | 2, 3, 4, 5, 6 |
| **6**       | 2, 3, 4, 5, 6 |
| **7**       | 1, 2, 3, 4, 5 |
| **8**       | 1, 2, 3, 4, 5 |
| **9**       | 1, 1, 2, 3, 4 |
| **10**      | 1, 1, 1, 2, 3 |

## Greats and Cheats

::: danger 
This section needs more work.
:::

### Greats
Greats will reduce the fatigue cost of a drill, and will often but not always increase the stat increase too.

#### Light Drills

#### Heavy Drills

### Cheats

