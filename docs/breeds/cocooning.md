# Cocooning

## Requirements

Cocooning is a very niche mechanic specific to Worms only. Any monster with the main breed Worm can cocoon. The requirements are as follows:

- Either E, D, or C Rank
- Between 4 and 5 years old
- Loyalty 80+
- Fatigue 0, Stress <= 7

If all of the above are true on June 4th, then your Worm will cocoon. If, in addition to all of the above requirements, you have **also** fed 30 Cup Jellies to your Worm, your Worm will cocoon into a Beaclon 100% of the time.

## Mechanics

When a worm cocoons, all of its underlying game data stays the same. This includes mostly superficial values, such as like and dislike, fear and spoil, nature, form etc., but also values such as the guts rate and arena movement speed. This makes cocooning a Worm/Pixie into a Gali/Worm or a Monol/Worm particularly desirable, as at best case you can achieve a Monol with a guts rate of 12, significantly better than the best possible "real" Monol, the Monol/Pixie, which has a guts rate of 14.

The monster will also receive a minor bonus, depending on what the result of the cocoon is.

| New Main Breed | Bonus           |
|----------------|-----------------|
| Pixie          | +30 Int         |
| Beaclon        | +30 Lif         |
| Hare           | +30 Pow         |
| Gali           | +30 Base Nature |
| Suezo          | +30 Int         |
| Jell           | +30 Speed       |
| Monol          | +30 Defence     |
| Naga           | +30 Skill       |

Additionally, when a Worm cocoons, its techs transform into techniques which belong to the new main breed. This can yield some interesting results, in some cases skipping tech chains and in others even removing basic techs. The transformation is always a direct 1-1 mapping, i.e. the same technique on a Worm will always turn into the same technique on the resulting monster.

Also of note is that special techs are in the table, despite cocooning only being available to Worms that are specifically below B rank, making it impossible to acquire any special techs prior to cocooning. The only way to acquire the cocooned special techs is to use one particular Worm available from a CD, which starts with Roll Assault.

| Initial Tech     | Pixie     | Beaclon            | Hare           | Gali                  | Suezo              | Jell           | Monol              | Naga | Initial Tech |
|------------------|-----------|--------------------|----------------|-----------------------|--------------------|-|---------------|--------------------|-|-|
| **Bite, Sting**  | Pat, Slap | Punch, Horn Strike | 1-2 Punch, Gas | Straight, Thunderbolt | Tail Assault, Spit |    Stab, Pierce | Charge, Flattening | Thwack, Belly Punch | **Bite, Sting**  |
| **Pierce**       | Kiss      | Tremor             | Foul Gas       | Typhoon               | Kiss               |    Fly Swatter  | Scratch            | Energy Shot         | **Pierce**       |
| **Poison Gas**   | Heel Raid | Tremor             | Bang           | Smash Thwack          | Lick               |    Slingshot    | Double Beams       | Poison Gas          | **Poison Gas**   |
| **Tusk Slash**   | Ray       | Horn Combo         | High Kick      | Flying Mask           | Telepathy          |    Beam Gun     | Strange Light      | Energy Shot         | **Tusk Slash**   |
| **Injection**    | Megaray   | Horn Cannon        | Spin Kick      | Cutting Mask          | Eye Beam           |    Beam Cannon  | Beam               | Energy Shots        | **Injection**    |
| **Pierce-Throw** | Flame     | Dive Assault       | Back Blow      | Thwack                | Bite               |    Jell Cube    | Needle Stabs       | Tail Assault        | **Pierce-Throw** |
| **Pinch-Throw**  | Heel Raid | Flying Press       | Rolling Blow   | Whirlwind             | Tongue Slap        |    Cannon       | Spike Stabs        | Drill Attack        | **Pinch-Throw**  |
| **Somersault**   | Bolt      | Heavy Punch        | Straight       | Back Blow             | Teleport           |    Whip         | Screech            | Stab                | **Somersault**   |
| **Somersaults**  | Lightning | Maximal Punch      | Hard Straight  | Fire Wall             | Telekinesis        |    Two Whips    | Flattening-X       | *                   | **Somersaults**  |
| **Tail Lash**    | Kick      | Horn Attack        | Kung-Fu Fist   | Blaze Wall            | Teleport           |    Jell Press   | Flattening-L       | Pierce              | **Tail Lash**    |
| **Tail Lashes**  | High Kick | Top Assault        | Kung-Fu Blow   | Heavy Blow            | Telekinesis        |    Jell Top     | Sound Wave         | *                   | **Tail Lashes**  |
| **Roll Assault** | Big Bang  | Horn Smash         | Heavy Smash    | Hurricane             | Chewing            |    Fly Smasher  | Three Knocks       | Turn Assault        | **Roll Assault** |

\* Indicates no technique is learned on carryover here 