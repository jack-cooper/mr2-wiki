# Lifespan and Life Type
Each monster breed has a lifespan and life type associated with it. These are 2 different concepts:

**Lifespan** - The total length of a time a breed lives for  
**Life type** - How the total lifespan is divided into each **life stage**

Life stages are what determine how effective training is for your monster, as well as how much you recover from rest. There are 10 total life stages, with the stat gains being roughly symmetrical, i.e. stage 1 has similar gains to stage 10, stage 3 has similar gains to stage 8 etc.

## Lifespan Lengths

Lifespan lengths are all multiples of 50 and are grouped as follows: 

### 350 Weeks
Dragon, Gali, Joker, Monol, Naga

### 400 Weeks
Beaclon, Centaur, Ducken, Ghost, Henger, Hopper, Pixie, Tiger, Undine, Worm

### 450 Weeks
Bajarl, Gaboo, Golem, Hare, Jell, Jill, Metalner, Mocchi, Niton, Phoenix, Suezo, Zilla, Zuum

### 500 Weeks
Ape, Arrowhead, Baku, ColorPandora, Durahan, Mew

### 550 Weeks
Kato, Mock, Plant

### 600 Weeks
Wracky

## Calculating Lifespan

A monster which has a different main and sub-breed will inherit 60% of its lifespan from its main breed
and the remaining 40% from its sub-breed, e.g. for an Ape/Gali:

Ape = 500 wks  
Gali = 350 wks  

500 * 0.6 = 300  
350 * 0.4 = 140  

Total lifespan = 300 + 140 = 440 wks

Another method that people have derived from the above calculations is to start at the main breed's lifespan, then add or subtract 20 weeks for each 50 week increment the sub-breed's lifespan is above or below the main breed's, e.g.

Ape = 500 wks  
Gali = 350 wks

To go from 500 to 350 we go down past 450, then 400, then to 350. This is **3** steps, so we subtract **3** * 20 weeks from Ape's base of 500 and we arrive at the correct answer of 440 weeks. This method will work for any monster.

## Life Types

There are only 4 life types. The life type of your monster is based only on its **sub**-breed.

### Life type 1

| Life Stage  |  Proportion |
|-------------|-------------|
|      1      | 10%         |
|      2      | 10%         |
|      3      | 15%         |
|      4      | 15%         |
|      5      | 10%         |
|      6      | 5%          |
|      7      | 10%         |
|      8      | 5%          |
|      9      | 5%          |
|      10     | 15%         |

Arrowhead, Bajarl, Centaur, Gali, Hare, Henger, Jell, Metalner, Mocchi, Monol, Zuum

### Life type 2

| Life Stage  |  Proportion |
|-------------|-------------|
|      1      | 5%          |
|      2      | 5%          |
|      3      | 10%         |
|      4      | 10%         |
|      5      | 5%          |
|      6      | 5%          |
|      7      | 15%         |
|      8      | 15%         |
|      9      | 10%         |
|      10     | 20%         |

Beaclon, Dragon, Ghost, Hopper, Mock, Naga, Pixie, Plant, Suezo, Tiger

### Life type 3

| Life Stage  |  Proportion |
|-------------|-------------|
|      1      | 15%         |
|      2      | 15%         |
|      3      | 15%         |
|      4      | 15%         |
|      5      | 10%         |
|      6      | 10%         |
|      7      | 5%          |
|      8      | 5%          |
|      9      | 5%          |
|      10     | 5%          |

Ape, Baku, ColorPandora, Gaboo, Golem, Jill, Kato, Niton, Phoenix, Worm, Wracky, Zilla

### Life type 4

| Life Stage  |  Proportion |
|-------------|-------------|
|      1      | 5%          |
|      2      | 5%          |
|      3      | 10%         |
|      4      | 10%         |
|      5      | 10%         |
|      6      | 5%          |
|      7      | 15%         |
|      8      | 15%         |
|      9      | 10%         |
|      10     | 15%         |

Ducken, Durahan, Joker, Mew, Undine

### Life type Table
All of the above information on life types condensed into a single table.

|             |  1  |  2  |  3  |  4  |
|-------------|-----|-----|-----|-----|
|      1      | 10% | 5%  | 15% | 5%  |
|      2      | 10% | 5%  | 15% | 5%  |
|      3      | 15% | 10% | 15% | 10% |
|      4      | 15% | 10% | 15% | 10% |
|      5      | 10% | 5%  | 10% | 10% |
|      6      | 5%  | 5%  | 10% | 5%  |
|      7      | 10% | 15% | 5%  | 15% |
|      8      | 5%  | 15% | 5%  | 15% |
|      9      | 5%  | 10% | 5%  | 10% |
|      10     | 15% | 20% | 5%  | 15% |