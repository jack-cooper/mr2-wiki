---
home: true
heroImage: /hero.png
heroText: Project EN-Wiki
tagline: The MR2 Guide
actionText: Get Started →
actionLink: breeds/breed-list.md
features:
  - title: Comprehensive MR2 Info
    details: This site intends to detail all of the important information about MR2 in a single easy to understand place.
  - title: This is a Work in Progress
    details: Every effort has been made to curate this information as best I can but there may be mistakes. Many sections are currently missing content and are being worked on.
  - title: It's a Community Effort!
    details: If you wish to reach out and help, visit the links in the navbar or reach out to me on Discord @Tsmuji#4177
footer: MIT Licensed | Made with VuePress
---
